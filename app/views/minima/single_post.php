<?php $this->view("minima/header",$data); ?>


<section class="section background-white">

    <div class="s-12 m-12 l-4 center">
        <h4 class="text-size-20 margin-bottom-20 text-dark text-center"><?=$data['post']->title ?></h4>
        <img src="<?=ROOT.$data['post']->image ?>" alt="">
        <br>
        <?=$data['post']->description ?>
    </div>
    <a href="<?=ROOT.'minima/home'?>" <input type="button" class=" submit-form button background-primary text-white" value="Back" >Back</a>
</section>


<?php $this->view("minima/footer",$data); ?>
