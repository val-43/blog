<?php $this->view("minima/header",$data); ?>

      <!-- MAIN -->

        <!-- Content -->
        <article>
          <header class="section background-white">
            <div class="line text-center">        
              <h1 class="text-dark text-s-size-30 text-m-size-40 text-l-size-headline text-thin text-line-height-1">Bienvenue sur mon Blog</h1>
              <p class="margin-bottom-0 text-size-16 text-dark">Ceci est un petit blog personnel, pensé tout d'abord pour être un terrain de jeu dédié à la programmation en PHP, des erreurs et des désigns hasardeux peuvent survenir, soyez indulgents ;)</p>
            </div>  
          </header>

          <?php if(is_array($data['posts'])): ?>
          <?php foreach($data['posts'] as $row): ?>

          <div class="background-white full-width">
            <div class="s-12 m-6 l-five">
              <a class="image-with-hover-overlay image-hover-zoom" href="<?=ROOT.'single_post/' . $row->url_address; ?>/" title="Portfolio Image">
                <div class="image-hover-overlay background-primary"> 
                  <div class="image-hover-overlay-content text-center padding-2x">
                    <h3 class="text-white text-size-20 margin-bottom-10"><?= $row->title ?></h3>
                    <p class="text-white text-size-14 margin-bottom-20"><?= $row->description ?></p>
                    <p class="text-white text-size-14 margin-bottom-20"><?= $row->date ?></p>
                  </div>
                </div> 
                <img class="full-img" src="<?=ROOT. $row->image ?>" alt=""/>
              </a>	
            </div>

            <?php endforeach;  ?>
            <?php endif; ?>

          </div>
        </article>
          <br>
            <a href="<?=$data['prev_page']; ?>"><input type="button" class=" submit-form button background-primary text-white" value="Prev"></a>
            <a href="<?=$data['next_page']; ?>"><input type="button" class=" submit-form button background-primary text-white" style="float: right" value="Next"></a>
        </section>
      </main>

<?php $this->view("minima/footer",$data); ?>
      
